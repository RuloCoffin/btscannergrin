package com.grin.rulo.btscanner.Adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.SortedList
import com.grin.rulo.btscanner.Core.BTLeItem
import com.grin.rulo.btscanner.R
import kotlinx.android.synthetic.main.ble_item_layout.view.*


class BTLeDiscoverAdapter(var context: Context?) :
    androidx.recyclerview.widget.RecyclerView.Adapter<BTLeDiscoverAdapter.BLeItemHolder>() {
    val inflater: LayoutInflater? = LayoutInflater.from(context)
    val btLeItems = SortedList<BTLeItem>(BTLeItem::class.java, object : SortedList.Callback<BTLeItem>() {
        override fun compare(o1: BTLeItem, o2: BTLeItem): Int {
            return o2.strenght.compareTo(o1.strenght)
        }

        override fun onChanged(position: Int, count: Int) {
            notifyItemRangeChanged(position, count)
        }

        override fun areContentsTheSame(oldItem: BTLeItem, newItem: BTLeItem): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areItemsTheSame(item1: BTLeItem, item2: BTLeItem): Boolean {
            return item1.address == item2.address
        }

        override fun onInserted(position: Int, count: Int) {
            notifyItemRangeInserted(position, count)
        }

        override fun onRemoved(position: Int, count: Int) {
            notifyItemRangeRemoved(position, count)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            notifyItemMoved(fromPosition, toPosition)
        }
    })

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BLeItemHolder {
        return BLeItemHolder(inflater!!.inflate(R.layout.ble_item_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return btLeItems.size()
    }

    override fun onBindViewHolder(holder: BLeItemHolder, position: Int) {
        holder.bind(btLeItems[position])
    }

    fun addBTLeItem(item: BTLeItem) {
        for (i in 0 until btLeItems.size()) {
            if (btLeItems.get(i).address == item.address) {
                Log.wtf("updating", btLeItems.get(i).toString())
                btLeItems.updateItemAt(i, item)
                return
            }
        }
        Log.wtf("adding", item.toString())
        btLeItems.add(item)
    }

    open inner class BLeItemHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        fun bind(item: BTLeItem) {
            itemView.txt_bt_item_name.text = item.address
            itemView.txt_bt_item_strength.text = item.name
        }
    }
}