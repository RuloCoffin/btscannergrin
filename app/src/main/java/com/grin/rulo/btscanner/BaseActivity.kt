package com.grin.rulo.btscanner


import android.bluetooth.BluetoothAdapter
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.grin.rulo.btscanner.Dialog.BTScannerDialogFragment


open class BaseActivity : AppCompatActivity() {
    private val TAG = BaseActivity::class.java.simpleName


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Phone does not support Bluetooth so let the user know and exit.
        if (BluetoothAdapter.getDefaultAdapter() == null || !packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            AlertDialog.Builder(this)
                .setTitle("Not compatible")
                .setMessage("Your phone does not support Bluetooth")
                .setPositiveButton("Exit") { _, _ -> finish() }
                .setIcon(R.drawable.ic_warning)
                .show()
        }
    }

}