package com.grin.rulo.btscanner.Core

import com.grin.rulo.btscanner.Core.Contants.Companion.STRING_EMPTY
import java.util.*

class BTLeItem(
    var address: String = STRING_EMPTY,
    var addedAt: Date? = null,
    var name: String? = STRING_EMPTY,
    var strenght: Int = 0


) {
    override fun toString(): String {
        return "BTLeItem(address='$address', addedAt=$addedAt, name=$name, strenght=$strenght)"
    }
}
