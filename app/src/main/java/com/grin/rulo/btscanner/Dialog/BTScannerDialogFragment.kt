package com.grin.rulo.btscanner.Dialog

import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.grin.rulo.btscanner.IStopScanListener
import com.grin.rulo.btscanner.R
import com.grin.rulo.btscanner.ScannerActivity
import kotlinx.android.synthetic.main.dialog_btscanner_spinner.*


class BTScannerDialogFragment : BaseDialogFragment() {
    private var currentItems = 0
    private var iStopScanListener: IStopScanListener? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_btscanner_spinner, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        btn_stop_scan.setOnClickListener {
            dismissDialog()
            iStopScanListener?.onStopClicked()
        }
    }

    companion object {
        fun newInstance(): BTScannerDialogFragment {
            val fragment = BTScannerDialogFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    fun addStopListener(iStopScanListener: IStopScanListener) {
        this.iStopScanListener = iStopScanListener
    }

    fun updateItemsFound(totalItems: Int) {
        val animator = ValueAnimator.ofInt(currentItems, totalItems)
        animator.duration = 200
        animator.addUpdateListener { animation -> txt_ble_items_found?.text = animation.animatedValue.toString() }
        if (txt_ble_items_found != null)
            animator.start()
        currentItems = totalItems
    }

}