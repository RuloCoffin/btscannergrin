package com.grin.rulo.btscanner


import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.grin.rulo.btscanner.Adapter.BTLeDiscoverAdapter
import com.grin.rulo.btscanner.Core.BTLeItem
import com.grin.rulo.btscanner.Core.Contants.Companion.SCAN_TIME
import com.grin.rulo.btscanner.Dialog.BTScannerDialogFragment

import kotlinx.android.synthetic.main.activity_client.*
import java.util.concurrent.TimeUnit


class ScannerActivity : BaseActivity(), IStopScanListener {
    override fun onStopClicked() {
        stopScan()
    }

    private var btScannerDialogFragment: BTScannerDialogFragment? = null

    private var mScanning: Boolean = false
    private var mHandler: Handler? = null
    private var mLogHandler: Handler? = null
    private var mScanResults: MutableMap<String, BluetoothDevice>? = null

    private var mBluetoothAdapter: BluetoothAdapter? = null
    private var mBluetoothLeScanner: BluetoothLeScanner? = null
    private var mScanCallback: ScanCallback? = null
    private lateinit var btLeDiscoverAdapter: BTLeDiscoverAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_client)
        mLogHandler = Handler(Looper.getMainLooper())
        val bluetoothManager = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothAdapter = bluetoothManager.adapter
        @SuppressLint("HardwareIds")
        val deviceInfo = ("Device Info"
                + "\nName: " + mBluetoothAdapter!!.name
                + "\nAddress: " + mBluetoothAdapter!!.address)

        txt_click_to_search.setOnClickListener { startScan() }
        //stop_scanning_button.setOnClickListener { stopScan() }
        (recycler_devices.getItemAnimator() as SimpleItemAnimator).supportsChangeAnimations = false
        recycler_devices.layoutManager = LinearLayoutManager(this@ScannerActivity)
        btLeDiscoverAdapter = BTLeDiscoverAdapter(this)
        recycler_devices.adapter = btLeDiscoverAdapter
    }

    override fun onResume() {
        super.onResume()

        // Check low energy support
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            // Get a newer device
            Log.wtf(TAG, "No LE Support.")
            finish()
        }
    }

    // Scanning

    private fun startScan() {
        if (!hasPermissions() || mScanning) {
            return
        }
        mScanResults = HashMap()
        showProgressAnim()
        mScanCallback = BtleScanCallback(mScanResults as HashMap<String, BluetoothDevice>)
        mBluetoothLeScanner = mBluetoothAdapter!!.bluetoothLeScanner
        val settings = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
            .build()
        mBluetoothLeScanner!!.startScan(null, settings, mScanCallback)
        mHandler = Handler()
        mHandler!!.postDelayed({
            this.stopScan()
            hideProgressAnim()
        }, (TimeUnit.SECONDS.toMillis(SCAN_TIME)))
        mScanning = true
        Log.wtf(TAG, "Started scanning.")
    }

    fun validateData() {
        if (btLeDiscoverAdapter.itemCount == 0) {
            txt_click_to_search.visibility = VISIBLE
            recycler_devices.visibility = GONE
        } else {
            if (txt_click_to_search.visibility == VISIBLE) {
                txt_click_to_search.visibility = GONE
                recycler_devices.visibility = VISIBLE
            }
        }
    }

    private fun stopScan() {
        if (mScanning && mBluetoothAdapter != null && mBluetoothAdapter!!.isEnabled && mBluetoothLeScanner != null) {
            mBluetoothLeScanner!!.stopScan(mScanCallback)
            scanComplete()
        }
        if (btLeDiscoverAdapter.itemCount == 0)
            txt_click_to_search.setText(R.string.lbl_no_devices_found)
        validateData()

        mScanCallback = null
        mScanning = false
        mHandler = null
        Log.wtf(TAG, "Stopped scanning.")
    }

    private fun scanComplete() {
        if (mScanResults!!.isEmpty()) {
            return
        }

        for (result in mScanResults!!.entries) {
            Log.wtf(TAG, result.key + "  " + result.value.toString())
        }
    }

    private fun hasPermissions(): Boolean {
        if (mBluetoothAdapter == null || !mBluetoothAdapter!!.isEnabled) {
            requestBluetoothEnable()
            return false
        } else if (!hasLocationPermissions()) {
            requestLocationPermission()
            return false
        }
        return true
    }

    private fun requestBluetoothEnable() {
        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        Log.wtf(TAG, "Requested user enables Bluetooth. Try starting the scan again.")
    }

    private fun hasLocationPermissions(): Boolean {
        return checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestLocationPermission() {
        requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_FINE_LOCATION)
        Log.wtf(TAG, "Requested user enable Location. Try starting the scan again.")
    }


    // Callbacks

    private inner class BtleScanCallback internal constructor(private val mScanResults: MutableMap<String, BluetoothDevice>) :
        ScanCallback() {


        override fun onScanResult(callbackType: Int, result: ScanResult) {
            addScanResult(result)
            val device = result.device
            btLeDiscoverAdapter.addBTLeItem(BTLeItem((device.address), null, device.name, result.rssi))
            btScannerDialogFragment?.updateItemsFound(btLeDiscoverAdapter.itemCount)
            validateData()
        }

        override fun onBatchScanResults(results: List<ScanResult>) {
            for (result in results) {
                addScanResult(result)
            }
        }

        override fun onScanFailed(errorCode: Int) {
            Log.wtf(TAG, "BLE Scan Failed with code $errorCode")
        }

        private fun addScanResult(result: ScanResult) {
            val device = result.device
            val deviceAddress = device.address
            mScanResults[deviceAddress] = device
        }
    }

    companion object {

        private val TAG = "ScannerActivity"

        private val REQUEST_ENABLE_BT = 1
        private val REQUEST_FINE_LOCATION = 2
    }

    private fun showProgressAnim() {
        btScannerDialogFragment = BTScannerDialogFragment.newInstance()
        btScannerDialogFragment?.addStopListener(this@ScannerActivity)
        btScannerDialogFragment?.show(supportFragmentManager, "BTScannerDialogFragment")
    }

    private fun hideProgressAnim() {
        try {
            if (btScannerDialogFragment != null)
                btScannerDialogFragment?.dismiss()
        } catch (x: Exception) {
            Log.wtf(TAG, x.message)
            x.printStackTrace()
        }

    }

    override fun onDestroy() {
        hideProgressAnim()
        super.onDestroy()
    }


}

interface IStopScanListener {
    fun onStopClicked()
}

