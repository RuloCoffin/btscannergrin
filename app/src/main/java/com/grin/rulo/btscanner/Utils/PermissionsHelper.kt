package com.grin.rulo.btscanner.Utils

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.*

class PermissionsHelper {
    enum class PERMISSIONS_RESULT {
        PERMISSIONS_OK,
        PERMISSIONS_FAIL
    }

    fun checkAndRequestPermissions(activity: Activity, requestCode: Int?, permissionsList: ArrayList<String>): Boolean {
        val permissionsMap = HashMap<String, Int>()
        val listPermissionsNeeded = ArrayList<String>()
        for (permission in permissionsList) {
            permissionsMap[permission] = checkSelfPermission(activity, permission)
        }
        for ((key, value) in permissionsMap) {
            if (value != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(key)
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toTypedArray(), requestCode!!)
            return false
        }
        return true
    }


    fun checkAndRequestPermissions(activity: androidx.fragment.app.FragmentActivity, requestCode: Int?, permissionsList: ArrayList<String>): Boolean {
        val permissionsMap = HashMap<String, Int>()
        val listPermissionsNeeded = ArrayList<String>()
        for (permission in permissionsList) {
            permissionsMap[permission] = checkSelfPermission(activity, permission)
        }
        for ((key, value) in permissionsMap) {
            if (value != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(key)
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toTypedArray(), requestCode!!)
            return false
        }
        return true
    }



    fun validateGrantedPermissions(permissions: Array<out String>, grantResults: IntArray, permissionsList: List<String>): PERMISSIONS_RESULT {
        val perms = HashMap<String, Int>()
        for (i in permissions.indices) {
            perms[permissions[i]] = grantResults[i]
        }

        for (permission in permissionsList) {
            if (perms.containsKey(permission))
                if (perms[permission] != PackageManager.PERMISSION_GRANTED)
                    return PERMISSIONS_RESULT.PERMISSIONS_FAIL
        }
        return PERMISSIONS_RESULT.PERMISSIONS_OK
    }
}